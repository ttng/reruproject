<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AjaxServiceController extends Controller
{
    public function getCourse(Request $request){
        $fac_id = $request->facid;
        $courses = \DB::table('courses')->where('fac_id', $fac_id)->get();
		return response()->json(['result'=>$courses]);
    }
}
