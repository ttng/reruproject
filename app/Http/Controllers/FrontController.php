<?php

namespace App\Http\Controllers;

use App\Author;
use App\Project;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    // public function index(){
    //     return view('front.index');
    // }
    public function file($id)
    {
        $file = Project::find($id);
        $name = $file->file;

        return response()->download(storage_path("app/{$name}"));

    }

   

   
   
   

    public function index(Request $request)
    {
        $error = "";
        $nodata = "";
        $radio = $request->input('group1');
        $keyword = $request->input('keyword');
        $projects = array();
        
        $texts = DB::table('textheader')->where('id',1)->get();
        // return view('front.master', compact('texts'));

        if($keyword){
            if ($radio == 'title' || $radio == 'year') {
                $results = Project::where($radio,'like','%'. $keyword.'%')->get(); 
                foreach($results as $result){
                  array_push($projects, $result);
                  }
           }elseif ($radio == 'author') {
              $authors = Author::where('name','like','%'. $keyword.'%')->get();
              foreach($authors as $author){
                  $result = Project::where('id',$author->project_id)->first();
                  array_push($projects, $result);
              }
           }
        }
        
        if ($projects == null && $keyword) {
            
            $nodata = "ไม่พบข้อมูลที่ตรงกัน";
            $error = "กรุณาตรวจสอบใหม่อีกครั้งค๊ะ ";

        }

        return view('front.master', compact('projects', 'keyword', 'radio', 'error', 'nodata','texts'));
    }

}
