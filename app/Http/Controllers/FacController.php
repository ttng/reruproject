<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use App\Faculty;
use Auth;
use App\User;

class FacController extends Controller
{
    public function __construct()
    {
    }

    public function updateCourse(Request $request)
    {
        $inputs = $request->all();
        
        $courses = Course::find($inputs['couid']);

        $courses->name = $inputs['couname'];

        $courses->save();

        return redirect('backend/addcourses/'.$courses->fac_id);
    }

    public function editCourse($id)
    {
        $user = Auth::user();
        if($user->role != 1){
            return redirect()->back();
        }
        
        $courses = Course::find($id);
        return view('backend.faculties.edit_courses', compact('courses'));
    }

    public function storeCourse(Request $request)
    {   
        $course = new Course;
        $course->name = $request->input('name');
        $course->fac_id = $request->input('facid');
        $course->save();
        return redirect('backend/addcourses/'.$course->fac_id);
    }
    public function addCourses($id)
    {
        // $tests = infor::where('name','like','%'.$name.'%')-> get();
        $courses = Course::where('fac_id',$id)->get();
        // $courses = Course::all();
        $faculties = Faculty::find($id);
        return view('backend.faculties.crate_courses', compact('faculties','courses'));
    }
    public function updateFac(Request $request)
    {
        $inputs = $request->all();
        $faculties = Faculty::find($inputs['facid']);
        $faculties->name = $inputs['name'];

        $faculties->save();
        return redirect('backend/showfaculties');
    }
    public function editFac($id)
    {
        $faculties = Faculty::find($id);
        return view('backend.faculties.edit_fac', compact('faculties'));
    }
    public function addFac()
    {

        return view('backend.faculties.crate_fac');
    }
    public function storeFaculties(Request $request)
    {
        $faculty = new Faculty;
        $faculty->name = $request->input('name');
        $faculty->save();
        return redirect('backend/showfaculties');
    }

    public function showFaculties()
    {
        $user = Auth::user();
        if($user->role != 1){
            return redirect()->back();
        }

        $faculties = Faculty::all();
        return view('backend.faculties.index', compact('faculties'));
    }
}
