<?php

namespace App\Http\Controllers;

use App\Author;
use App\Course;
use App\Faculty;
use App\Project;
use Auth;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function file($id){
        $file = Project::find($id);
        $name = $file->file;
        
        return response()->download(storage_path("app/{$name}"));

    }

     public function indexsearch(Request $request){
         
         $radio = $request->input('group1');
         $keyword = $request->input('keyword');

        $projects = array();
        if($keyword){
            if ($radio == 'title' || $radio == 'year') {
                $results = Project::where($radio,'like','%'. $keyword.'%')->get(); 
                foreach($results as $result){
                  array_push($projects, $result);
                  }
           }elseif ($radio == 'author') {
              $authors = Author::where('name','like','%'. $keyword.'%')->get();
              foreach($authors as $author){
                  $result = Project::where('id',$author->project_id)->first();
                  array_push($projects, $result);
              }
           }
        }
        
         return view('backend.show.index', compact('projects', 'keyword', 'radio'));
        
     }

    //  public function indexsearch(){
         
    //      return view('backend.show.index'); 
    //  }
     public function editProject($id){
        $project = Project::find($id);
        $authors = Author::where('project_id',$id)->get();
        return view('backend.project.edit_project', compact('project','authors'));

     }
     public function updateProject(Request $request){
        $inputs = $request->all();
        $project = project::find($inputs['pid']);
        $project->title = $inputs['title'];
        $project->save();

        $authors = Author::where('project_id', $project->id)->get();
        $i = 1;
        foreach($authors as $author){
            $author->name = $inputs['author'.$i++];
            $author->save();
        }
        
        return redirect('backend');
    }
    public function showProject(){
        $user = Auth::user();
        if($user->role == 1){
            $projects = Project::all();
        }else if($user->role == 2){
            $cid = $user->course_id;
            $projects = Project::where('courses_id', $cid)->get();
        }
        return view('backend.project.index', compact('projects'));
    }
    public function addProject()
    {
        $courses = Course::all();
        $faculties = Faculty::all();
        return view('backend.project.crate_project', compact('courses', 'faculties'));
        // return view('backend.project.crate_project');
    }
    public function storeProject(Request $request)
    {
        $user = Auth::user();
        $f_id = $user->fac_id; 
        $c_id = $user->course_id;

        if ($request->hasFile('file')) {
            
            $filename = $request->file->getClientOriginalname();
            $cname = Course::find($c_id)->name;

            $addfile = $request->file->storeAs('public/upload/'.$cname,$filename);

            $project = new Project;
            $project->title = $request->input('title');
            $project->file = $addfile;
            $project->year = $request->input('year');
            $project->faculties_id = $f_id;
            $project->courses_id = $c_id;
            $project->save();
            
            $author1 = $request->input('authornameone');
            $author2 = $request->input('authornametwo');
            if ($author2) {
                $author = new Author;
                $author->name = $author1;
                $author->project_id = $project->id;
                $author->save();

                $author = new Author;
                $author->name = $author2;
                $author->project_id = $project->id;
                $author->save();
            }
            else {
                $author = new Author;
                $author->name = $author1;
                $author->project_id = $project->id;
                $author->save();
            }

            return redirect('backend');
        }

    }

}
