<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\ProjectReport;
use Auth;

class ProjectReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->role == 1){
            $reports = ProjectReport::all();
        }else{
            $reports = ProjectReport::where('fac_id', $user->fac_id)->get();
        }
    
        return view('backend.projectReport.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.projectReport.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        if ($request->hasFile('file')) {
            $file = Input::file('file');
            $filename = $file->getClientOriginalname();
            $file->move('uploads/project', $filename);
            $path = "/uploads/project/".$filename;

            // $title = $input['name'];
            // $addfile = $request->file->storeAs('public/project/'.$title, $filename);
            
            $pr = new ProjectReport;
            $pr->name = $input['name'];
            $pr->reporter = $input['reporter'];
            $pr->fac_id = $input['fac_id'];
            $pr->course_id = $input['course_id'];
            $pr->report_date = $input['report_date'];
            $pr->file = $path;
            $pr->save();

            return redirect('backend/projectreport');

        }else{
            return redirect()->back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
