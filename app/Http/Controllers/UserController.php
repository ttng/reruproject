<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\User;
use Auth;
use Hash;
use App\Course;
use App\Faculty;

class UserController extends Controller
{
    public function attemplLogin(Request $request){
        $username = $request->input('username');
        $upass = $request->input('pass');
        $user = User::where('username', $username)->first();

        if($upass == $user->password){
            Auth::login($user);
            return redirect('backend');
        }else{
            return redirect()->back();
        }

    }

    public function getLogin(){
        return view('backend.auth.login');
    }

    public function getLogout(){
        Auth::logout();
        return redirect('/');
    }

    public function addUser(){
        $courses = Course::all();
        $faculties = Faculty::all();
        return view('backend.users.create_user', compact('courses', 'faculties'));
    }

    public function editUser($id){
        $user = User::find($id);
        return view('backend.users.edit_user', compact('user'));
    }

    public function showUsers(){
        $users = User::all();
        return view('backend.users.index', compact('users'));
    }
            // นำเข้าข้อมูล
    public function storeUser(Request $request){
        $user = new User;
        $user->username = $request->input('username');
        $user->password = $request->input('password');
        $user->fac_id = $request->input('fac_id');
        $user->course_id = $request->input('course_id');
        $user->save();
        return redirect('backend/showuser');
    }

    public function updateUser(Request $request){
        $inputs = $request->all();
        $user = User::find($inputs['uid']);
        $user->username = $inputs['username'];
        $user->password = $inputs['password'];
        $user->save();
        return redirect('backend/showuser');
    }
}
