@extends('backend.master')
@section('style')
    <style>
    .img-panel{
        background-color: white;
        border-radius: 10px;
        padding: 20px;
    }
    .img-panel .img-rounded{
        max-height: 50vh;
    }
    .pass-text{
        color:crimson;
    }
    </style>
@endsection
@section('content')
<div class="container-fluid">
  
        <div class="row page-titles">
            <div class="col-md-12 align-self-center">
                <h3 class="text-themecolor">Admin</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าแรก</a></li>
                    <li class="breadcrumb-item active">คณะ</li>
                </ol>
            </div>
        </div>
     
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>title</th>
                                                <th>ปีการศึกษา</th>
                                                <th>ผู้แต่ง</th>
                                                <th>สาขาวิชา</th>
                                                <th>คณะ</th>
                                                <th>โหลดไฟล์</th>
                                            </tr>
                                        </thead>
                                        {{-- php --}}
                                        <?php
                                            $i = 1;
                                        ?>
                                        <tbody>
                                            @foreach( $projects as $search)
                                    
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$search->title}}</td>
                                                <td>{{$search->year}}</td>
                                                <?php 
                                                $fac = DB::table('faculties')->where('id', $search->faculties_id)->first()->name;
                                                $course = DB::table('courses')->where('id', $search->courses_id)->first()->name;
                                                $authors = DB::table('author')->where('project_id',$search->id)->get();

                                                $color_code = array("#3b77ef", "#3befbf");
                                                $index = 0;
                                            ?>
                                                 <td>
                                                        @foreach($authors as $author)
                                                    <div style="color:{{$color_code[$index++]}}">{{$author->name}}</div>
        
                                                        @endforeach
                                                    </td>
                                                 <td>{{$course}}</td>
                                                 <td>{{$fac}}</td>
                                                
                                                <td>
                                                    <a href={{url('backend/show/'.$search->id)}} class="btn waves-effect waves-light btn-warning">
                                                    <i class="mdi mdi-account-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>

    @endsection

    @section('script')
    <script>
        $(function() {
            $('#show_menu').addClass('active');
        });
    </script>
    @endsection