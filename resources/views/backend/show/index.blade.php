@extends('backend.master')

@section('style')
    <style>
    .img-panel{
        background-color: white;
        border-radius: 10px;
        padding: 20px;
    }
    .img-panel .img-rounded{
        max-height: 50vh;
    }
    </style>
@endsection

@section('content')
{{-- _________________________________________________________________________________________________________________________________________________ --}}
<div class="container-fluid">
  
    <div class="row page-titles">
        <div class="col-md-12 align-self-center">
            <h3 class="text-themecolor">Admin</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                {{-- <li class="breadcrumb-item"><a href={{url('backend/showproject')}}>Project</a></li> --}}
                <li class="breadcrumb-item active">show</li>
            </ol>
        </div>
    </div>
 
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-block">
                    <form method="get" action="/backend/Search" enctype="multipart/form-data" class="form-horizontal form-material">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-12">ค้นหา</label>
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="text" class="form-control form-control-line" name="keyword" value="{{$keyword}}">
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success">
                                        <i class="mdi mdi-magnify" style="font-size:2vh"></i></button>
                                </div> 
                            </div>
                           
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-12" style="padding-bottom:18px">ค้นหาจาก</label>
                            <div class="row">
                                <div class="col-sm-8">
                                   <div class="demo-radio-button">
                                        <input name="group1" type="radio" class="with-gap" id="radio_1" value="title" @if($radio == 'title' || $radio == null)? checked @endif>
                                        <label for="radio_1">ชื่อเรื่อง</label>
                                        <input name="group1" type="radio" class="with-gap" id="radio_2" value="year" @if($radio == 'year')? checked @endif>
                                        <label for="radio_2">ปีการศึกษา</label>   
                                        <input name="group1" type="radio" class="with-gap" id="radio_3" value="author" @if($radio == 'author')? checked @endif>
                                        <label for="radio_3">ผู้แต่ง</label>                            
                                    </div> 
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>


    
    @if($projects)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>title</th>
                                                <th>ปีการศึกษา</th>
                                                <th>ผู้แต่ง</th>
                                                <th>สาขาวิชา</th>
                                                <th>คณะ</th>
                                                <th>โหลดไฟล์</th>
                                            </tr>
                                        </thead>
                                        {{-- php --}}
                                        <?php
                                            $i = 1;
                                        ?>
                                        <tbody>
                                            @foreach( $projects as $project)
                                    
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$project->title}}</td>
                                                <td>{{$project->year}}</td>
                                                <?php 
                                                    $fac = DB::table('faculties')->where('id', $project->faculties_id)->first()->name;
                                                    $course = DB::table('courses')->where('id', $project->courses_id)->first()->name;
                                                    $authors = DB::table('author')->where('project_id',$project->id)->get();

                                                    $color_code = array("#3b77ef", "#3befbf");
                                                    $index = 0;
                                                ?>
                                                <td>
                                                    @foreach($authors as $author)
                                                    <div style="color:{{$color_code[$index++]}}">{{$author->name}}</div>
                                                    @endforeach
                                                </td>
                                                <td>{{$course}}</td>
                                                <td>{{$fac}}</td>
                                                <td>
                                                    <a href={{url('backend/show/'.$project->id)}} class="btn waves-effect waves-light btn-warning">
                                                    <i class="mdi mdi-account-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    
</div>
@endsection

@section('script')
<script>
    $(function() {
        $('#show_menu').addClass('active');

    });
</script>
@endsection


        {{-- <div class="row">
          <div class="col-md-12">
              @foreach( $texe as $text)
          <h2>{{$text}}</h2>
          <h2> {{$keyword}}</h2>
          <h2>ในหมวดหมู่ {{$radio}}</h2>

          </div>
        </div>
    --}}