@extends('backend.master')

@section('style')
    <style>
    .img-panel{
        background-color: white;
        border-radius: 10px;
        padding: 20px;
    }
    .img-panel .img-rounded{
        max-height: 50vh;
    }
    .pass-text{
        color:crimson;
    }
    </style>
@endsection

@section('content')
<div class="container-fluid">
  
    <div class="row page-titles">
        <div class="col-md-12 align-self-center">
            <h3 class="text-themecolor">Admin</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Project</li>
            </ol>
        </div>
    </div>
 
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href={{url('backend/projectreport/create')}} class="btn waves-effect waves-light btn-primary">เพิ่ม</a>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>หัวเรื่อง</th>
                                            <th>ผู้รับผิดชอบ</th>
                                            <th>สาขาวิชา</th>
                                            <th>วันที่รายงาน</th>
                                            <th>File</th>
                                        </tr>
                                    </thead>
                                    <?php
                                        $i = 1;
                                    ?>
                                    <tbody>
                                        @foreach($reports as $report)
                                
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$report->name}}</td>
                                            <td>{{$report->reporter}}</td>
                                            <?php 
                                            
                                                $course = DB::table('courses')->where('id', $report->course_id)->first()->name;
                                                
                                            ?>
                                            <td>{{$course}}</td>
                                            <td>{{$report->report_date}}</td>
                                            <td>
                                                <a href="{{url($report->file)}}">file</a>
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        // $('#dashboard_menu').addClass('active');
    });
</script>
@endsection