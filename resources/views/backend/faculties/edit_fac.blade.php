@extends('backend.master')

@section('style')
    <style>
    .img-panel{
        background-color: white;
        border-radius: 10px;
        padding: 20px;
    }
    .img-panel .img-rounded{
        max-height: 50vh;
    }
    </style>
@endsection

@section('content')
<div class="container-fluid">
  
    <div class="row page-titles">
        <div class="col-md-12 align-self-center">
            <h3 class="text-themecolor">Admin</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item"><a href={{url('backend/showfaculties')}}>Faculties</a></li>
                <li class="breadcrumb-item active">Update</li>
            </ol>
        </div>
    </div>
 
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-block">
                    <form method="post" action="/backend/updatefac" class="form-horizontal form-material">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-12">คณะ</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" name="name" value={{$faculties->name}}>
                            </div>
                        </div>          
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div> 
                        <input type="hidden" name="facid" value="{{$faculties->id}}">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        $('#fac_menu').addClass('active');
    });
</script>
@endsection