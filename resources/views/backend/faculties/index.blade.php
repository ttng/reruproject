@extends('backend.master')
@section('style')
    <style>
    .img-panel{
        background-color: white;
        border-radius: 10px;
        padding: 20px;
    }
    .img-panel .img-rounded{
        max-height: 50vh;
    }
    .pass-text{
        color:crimson;
    }
    </style>
@endsection
@section('content')
<div class="container-fluid">
  
        <div class="row page-titles">
            <div class="col-md-12 align-self-center">
                <h3 class="text-themecolor">Admin</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าแรก</a></li>
                    <li class="breadcrumb-item active">คณะ</li>
                </ol>
            </div>
        </div>
     
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <a href={{url('backend/addfac')}} class="btn waves-effect waves-light btn-primary">เพิ่ม</a>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>คณะ</th>
                                                <th>แก้ไข</th>
                                                <th>จัดการสาขาวิชา</th>
                                            </tr>
                                        </thead>
                                        {{-- php --}}
                                        <?php
                                            $i = 1;
                                        ?>
                                        <tbody>
                                            @foreach( $faculties as $facultie)
                                    
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$facultie->name}}</td>
                                               
                                                
                                                
                                                <td><a href={{url('backend/editfac/'.$facultie->id)}} class="btn waves-effect waves-light btn-warning">
                                                    <i class="mdi mdi-account-edit"></i>
                                                    </a>
                                                </td>
                                                <td><a href={{url('backend/addcourses/'.$facultie->id)}} class="btn waves-effect waves-light btn-warning">
                                                    <i class="mdi mdi-account-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>

@endsection
@section('script')
<script>
    $(function() {
        $('#fac_menu').addClass('active');
    });
</script>
@endsection