@extends('backend.master')
@section('content')
<div class="container-fluid">
  
        <div class="row page-titles">
            <div class="col-md-12 align-self-center">
                <h3 class="text-themecolor">Admin</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าแรก</a></li>
                    <li class="breadcrumb-item"><a href={{url('backend/showfaculties')}}>คณะ</a></li>
                    <li class="breadcrumb-item active">เพื่มข้อมูลคณะ</li>
                </ol>
            </div>
        </div>
     {{-- card form --}}
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-block">
                        <form method="post" action="/backend/storefaculties" class="form-horizontal form-material">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-md-12">name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="name">
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $(function() {
        $('#fac_menu').addClass('active');
    });
</script>
@endsection