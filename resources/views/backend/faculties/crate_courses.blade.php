@extends('backend.master')

@section('style')
    <style>
    .img-panel{
        background-color: white;
        border-radius: 10px;
        padding: 20px;
    }
    .img-panel .img-rounded{
        max-height: 50vh;
    }
    .textfaculties{
        color:red;
    }
    
    </style>
@endsection

@section('content')

  
    <div class="row page-titles">
        <div class="col-md-12 align-self-center">
            <h3 class="text-themecolor">Admin</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item"><a href={{url('backend/showfaculties')}}>Faculties</a></li>
                <li class="breadcrumb-item active">CrateCourses</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
            <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                {{-- <a href={{url('backend/addfac')}} class="btn waves-effect waves-light btn-primary">เพิ่ม</a> --}}
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <h4 class="textfaculties">คณะ  {{$faculties->name}}</h4> <br/>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>จำนวนสาขาวิชา</th>
                                                <th>ชื่่อสาขาวิชา</th>
                                                <th>แก้ไข</th>
                                                {{-- <th>เพื่มสาขาวิชา</th> --}}
                                            </tr>
                                        </thead>
                                        {{-- php --}}
                                        <?php
                                            $i = 1;
                                        ?>
                                        <tbody>
                                            @foreach( $courses as $course)
                                    
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$course->name}}</td>
                                               
                                                
                                                
                                                <td><a href={{url('backend/editcourse/'.$course->id)}} class="btn waves-effect waves-light btn-warning">
                                                    <i class="mdi mdi-account-edit"></i>
                                                    </a>
                                                </td>
                                               
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-block">
                    <form method="post" action="/backend/storecourses" class="form-horizontal form-material">
                        {{ csrf_field() }}
                        {{-- <div class="form-group">
                            <label class="col-md-12">คณะ</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" name="facname" value={{$faculties->name}}>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <label class="col-md-12">เพื่มสาขา</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" name="name" >
                            </div>
                        </div>                   
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div> 
                        <input type="hidden" name="facid" value="{{$faculties->id}}">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        $('#fac_menu').addClass('active');
    });
</script>
@endsection