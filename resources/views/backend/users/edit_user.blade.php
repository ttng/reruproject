@extends('backend.master')

@section('style')
    <style>
    .img-panel{
        background-color: white;
        border-radius: 10px;
        padding: 20px;
    }
    .img-panel .img-rounded{
        max-height: 50vh;
    }
    </style>
@endsection

@section('content')
<div class="container-fluid">
  
    <div class="row page-titles">
        <div class="col-md-12 align-self-center">
            <h3 class="text-themecolor">Admin</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item"><a href={{url('backend/showuser')}}>Users</a></li>
                <li class="breadcrumb-item active">Update</li>
            </ol>
        </div>
    </div>
 
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-block">
                    <form method="post" action="/backend/updateuser" class="form-horizontal form-material">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-12">Username</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" name="username" value={{$user->username}}>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-12">เปลี่ยน Password</label>
                            <div class="col-md-12">
                                <input type="password" class="form-control form-control-line" name="password" placeholder="xxxx">
                            </div>
                        </div>
               
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div> 
                        <input type="hidden" name="uid" value="{{$user->id}}">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        $('#user_menu').addClass('active');
    });
</script>
@endsection