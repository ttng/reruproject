@extends('backend.master')

@section('style')
    <style>
    .img-panel{
        background-color: white;
        border-radius: 10px;
        padding: 20px;
    }
    .img-panel .img-rounded{
        max-height: 50vh;
    }
    .pass-text{
        color:crimson;
    }
    </style>
@endsection

@section('content')
<div class="container-fluid">
  
    <div class="row page-titles">
        <div class="col-md-12 align-self-center">
            <h3 class="text-themecolor">Admin</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Users</li>
            </ol>
        </div>
    </div>
 
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href={{url('backend/adduser')}} class="btn waves-effect waves-light btn-primary">เพิ่ม</a>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Username</th>
                                            <th>Password</th>
                                            <th>ประเภท</th>
                                            <th>คณะ</th>
                                            <th>สาขาวิชา</th>
                                            <th>เข้าใช้ล่าสุด</th>
                                            <th>แก้ไข</th>
                                        </tr>
                                    </thead>
                                    <?php
                                        $i = 1;
                                    ?>
                                    <tbody>
                                        @foreach($users as $user)
                                
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$user->username}}</td>
                                            <td class="pass-text">{{$user->password}}</td>
                                            <?php
                                                $user_role = '';
                                                if($user->role == 1){
                                                    $user_role = 'Admin';
                                                }else{
                                                    $user_role = 'ผู้ใช้งาน';
                                                }

                                                $fac = DB::table('faculties')->where('id', $user->fac_id)->first()->name;
                                                $course = DB::table('courses')->where('id', $user->course_id)->first()->name;
                                                
                                            ?>
                                            <td>{{$user_role}}</td>
                                            <td>{{$fac}}</td>
                                            <td>{{$course}}</td>
                                            <td>{{$user->updated_at}}</td>
                                            <td><a href={{url('backend/edituser/'.$user->id)}} class="btn waves-effect waves-light btn-warning">
                                                <i class="mdi mdi-account-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        $('#user_menu').addClass('active');
    });
</script>
@endsection