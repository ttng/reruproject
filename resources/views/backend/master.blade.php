<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/material-lite/assets/images/favicon.png">
    <title>Admin</title>
    <!-- Bootstrap Core CSS -->
    <link href="/material-lite/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
    <!-- chartist CSS -->
    {{-- <link href="/material-lite/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="/material-lite/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="/material-lite/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet"> --}}
    <!--This page css - Morris CSS -->
    <link href="/material-lite/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/material-lite/lite/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/material-lite/lite/css/colors/blue.css" id="theme" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
    @yield('style')

    <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">

    <style>
        body{
            font-family: 'Kanit', sans-serif;
        }
        h3, h4, h5, h6{
            font-family: 'Kanit', sans-serif;
        }
        .admin-theme{
            background-color: grey;
        }
        
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            
                            <!-- Light Logo icon -->
                            <img src="/material-lite/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         
                         <!-- Light Logo text -->    
                         {{-- <img src="/material-lite/assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a> --}}
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0 admin-theme">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
    
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="{{url('backend/getlogout')}}" aria-haspopup="true" aria-expanded="false">
                                {{Auth::user()->username}}
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="waves-effect waves-dark" id="dashboard_menu" href="{{url('/backend')}}" aria-expanded="false">
                            <i class="mdi mdi-gauge"></i>
                            <span class="hide-menu">Dashboard</span>
                        </a>
                        </li>
                        <li> <a class="waves-effect waves-dark" id="show_menu" href="{{url('/backend/Search')}}" aria-expanded="false">
                            <i class="mdi mdi-account-edit"></i>
                            <span class="hide-menu">ค้นหาหนังสือ</span>
                        </a>
                        </li>

                        <li> 
                            <a class="waves-effect waves-dark" id="project_report_menu" href="{{url('/backend/projectreport')}}" aria-expanded="false">
                                <i class="mdi mdi-account-edit"></i>
                                <span class="hide-menu">รายงานผลโครงการ</span>
                            </a>
                        </li>

                        @if(Auth::user()->role == 1)
                        <li> <a class="waves-effect waves-dark" id="fac_menu" href="{{url('/backend/showfaculties')}}" aria-expanded="false">
                            <i class="mdi mdi-school"></i>
                            <span class="hide-menu">จัดการคณะ</span>
                            </a>
                        </li>
                        
                        <li> <a class="waves-effect waves-dark" id="user_menu" href="{{url('/backend/showuser')}}" aria-expanded="false">
                            <i class="mdi mdi-account"></i>
                            <span class="hide-menu">จัดการผู้ใช้</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            {{-- <div class="sidebar-footer">
                    
                <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="mdi mdi-facebook-box"></i></a>
                <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <a href={{url('getlogout')}} class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
            </div> --}}
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
           
            @yield('content')

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © คณะเทคโนโลยีสารสนเทศ </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/material-lite/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/material-lite/assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="/material-lite/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="/material-lite/lite/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="/material-lite/lite/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="/material-lite/lite/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="/material-lite/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="/material-lite/lite/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    {{-- <script src="/material-lite/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="/material-lite/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script> --}}
    <!--c3 JavaScript -->
    <script src="/material-lite/assets/plugins/d3/d3.min.js"></script>
    <script src="/material-lite/assets/plugins/c3-master/c3.min.js"></script>
    <!-- Chart JS -->
    <script src="/material-lite/lite/js/dashboard1.js"></script>

    @yield('script')
</body>

</html>
