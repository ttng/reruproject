@extends('backend.master')

@section('style')
    <style>
    .img-panel{
        background-color: white;
        border-radius: 10px;
        padding: 20px;
    }
    .img-panel .img-rounded{
        max-height: 50vh;
    }
    </style>
@endsection

@section('content')
<div class="container-fluid">
  
    <div class="row page-titles">
        <div class="col-md-12 align-self-center">
            <h3 class="text-themecolor">Admin</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item"><a href={{url('backend/showproject')}}>Project</a></li>
                <li class="breadcrumb-item active">เพื่มโครงการ</li>
            </ol>
        </div>
    </div>
 
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-block">
                    <form method="post" action="/backend/storeproject" enctype="multipart/form-data" class="form-horizontal form-material">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-12">หัวเรื่อง</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" name="title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-md-12">ผู้แต่งคนที่1</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" name="authornameone">
                            </div>
                        </div>
                        <div class="form-group">
                                <label  class="col-md-12">ผู้แต่งคนที่2</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="authornametwo">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Default file upload</label>
                                <input type="file" class="form-control" name="file" aria-describedby="fileHelp">
                            </div>
                            <div class="form-group">
                                <label  class="col-md-12">ภาคเรียนที่</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="year" placeholder="กรอกเทอมและปีการศึกษาที่ส่งรูปเล่ม ** 2/25xx **">
                                </div>
                            </div>
                        {{-- <div class="form-group">
                            <label class="col-sm-12">Role</label>
                            <div class="col-sm-12">
                                <select name="role" class="form-control form-control-line">
                                    <option value="staff">Staff</option>
                                    <option value="admin">Admin</option>
                                </select>
                            </div>
                        </div> --}}
                        {{-- <div class="form-group">
                            <label class="col-sm-12">คณะ</label>
                            <div class="col-sm-12">
                                <select name="fac_id" class="form-control form-control-line" id="fac_id">
                                        <option value=0>--เลือกคณะ--</option>
                                    @foreach($faculties as $fac)
                                        <option value="{{$fac->id}}">{{$fac->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">สาขาวิชา</label>
                            <div class="col-sm-12">
                                <select name="course_id" class="form-control form-control-line" id="course_id">
                                        <option value=0>--เลือกสาขาวิชา--</option>
                                    @foreach($courses as $course)
                                        <option value="{{$course->id}}">{{$course->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        $('#dashboard_menu').addClass('active');

        var searchRequest = null;
        $("#fac_id").change(function(e){  
        var value = $("#fac_id").val();
            e.preventDefault();
            searchRequest = $.ajax({
                type: "get",
                url: "/get_course_list",
                data: { facid: value, access_token: $("access_token").val() },
                success:function(data){
                    console.log('สาขา',data.result);
                    $("#course_id").find('option').remove();
                    $("#course_id").append(
                    '<option value="0" selected="selected">--เลือกสาขาวิชา--</option>'
                    );
                    $.each(data.result, function(index, course){
                        var cid = course.id;
                        var cname = course.name;
                        $("#course_id").append(
                        '<option value="'+ cid +'">'+ cname +'</option>'
                        );
                    });
                }      
            });
        });
    });
</script>
@endsection