<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>RERU Project Lib</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <!-- Favicons -->
  <link href="/Reveal/img/favicon.png" rel="icon">
  <link href="/Reveal/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="/Reveal/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="/Reveal/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/Reveal/lib/animate/animate.min.css" rel="stylesheet">
  <link href="/Reveal/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="/Reveal/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="/Reveal/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="/Reveal/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/css/radio.css">
  <link href="/material-lite/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
  <link href="/material-lite/lite/css/style.css" rel="stylesheet">
  <link href="/material-lite/lite/css/colors/blue.css" id="theme" rel="stylesheet">
  <link href="/public/font/fontth.css" id="theme" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
  <!-- Main Stylesheet File -->
  <link href="/Reveal/css/style.css" rel="stylesheet">

  
  <style>
      body{
          font-family: 'Kanit', sans-serif;
      }
      h2, h3, h4, h5, h6{
          font-family: 'Kanit', sans-serif;
      }
      .admin-theme{
          background-color: grey;
      }
      
  </style>

  <!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body id="body">

  <!--==========================
    Header
  ============================-->
  <header id="header">
    
    <div class="container">
      <div id="logo" class="pull-left" >
        
        <h1><a href="#body" class="scrollto" style="color:#0474F3">RE<span style="color:#FB8500">RU</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <div class="row">
            <div class="col-md-11">

            </div>
            <div class="col-md-1">
              <li class="menu-active">  <a href="{{url('backend')}}">เข้าสูระบบ</a>
              </div>
          </div>
          
          
        </ul>
        
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

 
  <section id="intro">

    <div class="intro-content body" >
      <h2 style="color:#0474F3">ระบบ <span>คลังข้อมูลโครงการนักศึกษา</span><br><br>
         มหาวิทยาลัยราชภัฏร้อยเอ็ด</span>
      </h2>
    </div>

    <div id="intro-carousel" class="owl-carousel" >
      
      <div class="item" style="background-image: url('/public/Reveal/img/intro-carousel/1.jpg');"></div>
      <div class="item" style="background-image: url('/public/Reveal/img/intro-carousel/2.jpg');"></div>
      <div class="item" style="background-image: url('/public/Reveal/img/intro-carousel/3.jpg');"></div>
      {{-- <div class="item" style="background-image: url('/Reveal/img/');"></div>
      <div class="item" style="background-image: url('/Reveal/img/');"></div>
      <div class="item" style="background-image: url('/Reveal/img/');"></div>
      <div class="item" style="background-image: url('/Reveal/img/');"></div>
      <div class="item" style="background-image: url('/Reveal/img/');"></div>
      <div class="item" style="background-image: url('/Reveal/img/');"></div> --}}
    </div>

  </section><!-- #intro -->

<main id="main" class="body">
    {{-- __________________________________________________________________________________________________________________________________ --}}
    <section id="portfolio" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          @if($projects)
          <?php
          $num = count($projects)
          ?>
          <div class="row">
            <h3>รายการทั้งหมด{{$num}}</h3>
          </div>
           
          @elseif ($error && $nodata)
            <div class="row">
            <div class="col-md-4">
              
            </div>
            <div class="col-md-4">
                <h3 style="color:#F71705">*{{$nodata}}*</h3>
             </div>
             <div class="col-md-4">
                
             </div>
          </div>
          @endif
          
        </div>
      </div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
              <img src="/Reveal/img/portfolio/1.jpg" alt="" style="width:100%">
        </div>
            <div class="col-md-6">
                <div class="form">
            {{-- <form method="get" action="{{url('/index')}} "  class="contactForm"> --}}
                   <form method="GET" action="{{url('/')}}" >
                    <a href={{url('/new')}}></a>
                        <div class="form-row">
                          <label class="col-md-12">ค้นหา</label>
                            <div class="row">
                                <div class="form-group col-md-10">
                                      <input type="text"   value="{{$keyword}}" class="form-control"  name="keyword" >
                                </div> 
                                    <div class="col-md-2">
                                      <button type="submit" class="btn btn-success">
                                      <i class="mdi mdi-magnify" style="font-size:2vh"></i></button>
                                    </div> 
                            </div>
                        </div>
                      {{-- </form> --}}
              
              <div class="form-group">
                  <label class="col-md-12" style="padding-bottom:9px">ค้นหาจาก</label>
                  <div class="row">
                      <div class="col-sm-12">
                         <div class="demo-radio-button">
                              <input name="group1" type="radio" class="with-gap" id="radio_1" value="title"  checked >
                              <label for="radio_1">ชื่อเรื่อง</label>
                              <input name="group1" type="radio" class="with-gap" id="radio_2" value="year" >
                              <label for="radio_2">ปีการศึกษา</label>   
                              <input name="group1" type="radio" class="with-gap" id="radio_3" value="author">
                              <label for="radio_3">ผู้แต่ง</label>                            
                          </div> 
                      </div>
                      <div class="col-sm-4"></div>
                  </div>
                  </div>
            </form>
            </div>
              </div>
              <div class="col-md-2"></div>
            </div>
      </div>
      
    {{-- @if($keyword === null ) --}}
     
      @if($projects)
     
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>title</th>
                                                <th>ปีการศึกษา</th>
                                                <th>ผู้แต่ง</th>
                                                <th>สาขาวิชา</th>
                                                <th>คณะ</th>
                                                <th>โหลดไฟล์</th>
                                            </tr>
                                        </thead>
                                        {{-- php --}}
                                        <?php
                                        $i=1
                                        ?>
                                        
                                        <tbody>
                                            @foreach( $projects as $project)
                                    
                                            <tr>
                                              {{-- <td>1</td>
                                              <td>2</td>
                                              <td>3</td> --}}
                                                <td>{{$i++}}</td>
                                                <td>{{$project->title}}</td>
                                                <td>{{$project->year}}</td>
                                                <?php 
                                                    $fac = DB::table('faculties')->where('id', $project->faculties_id)->first()->name;
                                                    $course = DB::table('courses')->where('id', $project->courses_id)->first()->name;
                                                    $authors = DB::table('author')->where('project_id',$project->id)->get();
                                                    // $error = DB::table('error')->get();
                                                    $color_code = array("#3b77ef", "#3befbf");
                                                    $index = 0;
                                                    // if($keyword != null){
                                                    // if ($fac = null) {
                                                      // $error = DB::table('error')->get();
                                                  //   }
                                                  // }
                                                ?>
                                                <td>
                                                    @foreach($authors as $author)
                                                    <div style="color:{{$color_code[$index++]}}">{{$author->name}}</div>
                                                    @endforeach
                                                </td>
                                                <td>{{$course}}</td>
                                                <td>{{$fac}}</td>
                                             
                                                <td> {{--btn download file --}}
                                                    <a href={{url('backend/show/'.$project->id)}} class="btn waves-effect waves-light btn-warning">
                                                    <i class="mdi mdi-account-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>          
            
    @endif
@if ($error && $nodata)
<div class="row">
    <div class="col-md-4">  
</div>
  <div class="col-md-4">
  <h1>{{$error}}</h1>
  </div>
  <div class="col-md-4">
   
  </div>

</div>
@endif
    
   
        
     
    </section><!-- #portfolio -->

    
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; คณะเทคโนโลยีสารสนเทศ <strong>สาขาวิทยาการคอมพิวเตอร์</strong> 2019
      </div>
      <div class="credits">
       
         <a href="https://www.reru.ac.th/">มหาวิทยาลัยราชภัฏร้อยเอ็ด</a>
      </div>
    </div>
  </footer>

  {{-- <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a> --}}

  <!-- JavaScript Libraries -->
  <script src="/Reveal/lib/jquery/jquery.min.js"></script>
  <script src="/Reveal/lib/jquery/jquery-migrate.min.js"></script>
  <script src="/Reveal/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/Reveal/lib/easing/easing.min.js"></script>
  <script src="/Reveal/lib/superfish/hoverIntent.js"></script>
  <script src="/Reveal/lib/superfish/superfish.min.js"></script>
  <script src="/Reveal/lib/wow/wow.min.js"></script>
  <script src="/Reveal/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="/Reveal/lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="/Reveal/lib/sticky/sticky.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="/Reveal/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="/Reveal/js/main.js"></script>

</body>
</html>
