<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', function(){
    return redirect('index'); 
});

Route::group(['prefix' => 'index'], function() {
    Route::get('/','FrontController@index');
    Route::post('/updatatext','FrontController@updatatextheader');  
    Route::get('/download','FrontController@file'); 
});

Route::get('/new', 'DashboardController@index');

Route::get('/getlogin', 'UserController@getLogin');
Route::post('/attemptlogin', 'UserController@attemplLogin');
Route::get('/get_course_list', 'AjaxServiceController@getCourse');

Route::group(['prefix' => 'backend', 'middleware'=>'admin-auth'], function() {
    Route::get('/', 'ProjectController@showProject');
    Route::get('/getlogout', 'UserController@getLogout');

    Route::get('/Search','ProjectController@indexsearch');
    Route::get('/Searchs','ProjectController@search');
    Route::get('/show/{id}','ProjectController@file');

    Route::get('/showuser', 'UserController@showUsers');
    Route::get('/adduser', 'UserController@addUser');
    Route::post('/storeuser', 'UserController@storeUser');
    Route::get('/edituser/{uid}', 'UserController@editUser');
    Route::post('/updateuser', 'UserController@updateUser');

    Route::get('/showfaculties', 'FacController@showFaculties');
    Route::get('/addfac', 'FacController@addFac');
    Route::post('/storefaculties', 'FacController@storeFaculties');
    Route::get('/editfac/{facid}', 'FacController@editFac');
    Route::post('/updatefac', 'FacController@updateFac');

    Route::get('/addcourses/{facid}','Faccontroller@addCourses');
    Route::post('/storecourses','Faccontroller@storeCourse');
    Route::get('/editcourse/{conid}', 'FacController@editCourse');
    Route::post('/updatecourse', 'FacController@updateCourse');

    Route::get('/addproject', 'ProjectController@addProject');
    Route::post('/storeproject', 'ProjectController@storeProject');
    Route::get('/editproject/{pid}','ProjectController@editProject');
    Route::post('/updateproject', 'ProjectController@updateProject');

    Route::resource('projectreport', 'ProjectReportController');

    

});
